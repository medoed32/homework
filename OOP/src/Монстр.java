public class Монстр {
    int eyes, legs, hands;

    Монстр(){
        eyes = 2;
        legs = 2;
        hands = 2;
    }

    Монстр(int eyes){
        this.eyes = eyes;
        legs = 2;
        hands = 2;
    }

    Монстр(int eyes, int legs){
        this.eyes = eyes;
        this.legs = legs;
        hands = 2;
    }

    Монстр(int eyes, int legs, int hands){
        this.eyes = eyes;
        this.legs = legs;
        this.hands = hands;
    }

    static void voice(){
        System.out.println("Голос");
    }

    static void voice(int i){
        while (i > 0)
        {
            i--;
            System.out.print("Голос");
            if(i == 0)
            {
                System.out.print(".");
            }
            else
            {
                System.out.print(", ");
            }
        }
    }
    static void voice(int i, String word)
    {
        while(i > 0)
        {
            i--;
            System.out.print(word);
            if(i == 0)
            {
                System.out.print(".");
            }
            else
            {
                System.out.print(", ");
            }

        }
    }
}
