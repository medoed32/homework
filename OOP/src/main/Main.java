package main;

import box.Box;
import box.BoxWeight;

public class Main {
    public static void main(String[] args) {

        Box box = new Box(20);
        BoxWeight boxWeight = new BoxWeight(10, 20, 30, 12);
        box.showInfo();
        boxWeight.showInfo();

    }
}
