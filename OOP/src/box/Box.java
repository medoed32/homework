package box;

public class Box {
    private double width;
    private double height;
    private double length;

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public Box(double width, double height, double length) {
        this.width = width;
        this.height = height;
        this.length = length;
    }

    public Box(double size) {
        width = size;
        height = size;
        length = size;
    }

    public Box() {
        width = 0;
        height = 0;
        length = 0;
    }

    public Box(Box box) {
        this.width = box.width;
        this.height = box.height;
        this.length = box.length;
    }

    public Box intcrease(int i) {
        return new Box(width * i, height * i, length * i);
    }

    public void setDimens(double width, double height, double length) {
        this.width = width;
        this.height = height;
        this.length = length;
    }

    public double Volume() {
        return width * height * length;
    }

    public void showVolume() {
        System.out.println(Volume());
    }

    public int compare(Box box) {
        double thisVolume = Volume();
        double boxVolume = box.Volume();

        if (thisVolume > boxVolume) {
            return 1;
        } else if (thisVolume < boxVolume) {
            return -1;
        } else {
            return 0;
        }
    }

    //HomeWork17
    public Box(Box box1, Box box2) {
        this.width = box1.width + box2.width;
        this.height = box1.height + box2.height;
        this.length = box1.length + box2.length;
    }

    public double vozvrat(Box box) {
        double box1 = this.Volume();
        return box1 = box1 + this.length * this.length * this.length;
    }

    public void showInfo() {

    }


}
