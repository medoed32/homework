public class Rect {
    double width;
    double length;

    void RectReset (double width, double length) {
        this.width = width;

        this.length = length;
    }

    double Perimetr() {
        return width * 2 + length * 2;
    }

    double Square() {
        return width * length;
    }
}
