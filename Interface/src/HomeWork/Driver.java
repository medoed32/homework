package HomeWork;

public class Driver extends Worker implements Driveble {
    public Driver(String name) {
        super(name);
    }

    @Override
    public void voice() {
        System.out.println("Водитель " + getName());
    }

    @Override
    public void drive() {
        System.out.println("Водить автомобиль");
    }
}
