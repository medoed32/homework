package HomeWork;

public class Programmer extends Worker implements Programmble {
    public Programmer(String name) {
        super(name);
    }

    @Override
    public void voice() {
        System.out.println("Программист " + getName());
    }

    @Override
    public void writeCode() {
        System.out.println("Писать код");
    }
}
