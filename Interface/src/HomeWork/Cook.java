package HomeWork;

public class Cook extends Worker implements Cookble {
    public Cook(String name) {
        super(name);
    }

    @Override
    public void voice() {
        System.out.println("Повар " + getName());
    }

    @Override
    public void cook() {
        System.out.println("Готовить кушать");
    }
}
