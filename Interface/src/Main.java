import HomeWork.*;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Worker programmer = new Programmer("Дарья");
        Worker cooker = new Cook("Иван");
        Worker driver = new Driver("Константин");
        ArrayList<Worker> workers = new ArrayList<>();
        workers.add(programmer);
        workers.add(cooker);
        workers.add(driver);

        for (Worker worker : workers) {
            worker.voice();
        }

        ArrayList<Programmble> programmbles = new ArrayList<>();
        programmbles.add((Programmble) programmer);

        for (Programmble programmble : programmbles) {
            programmble.writeCode();
        }

        ArrayList<Cookble> cookbles = new ArrayList<>();
        cookbles.add((Cookble) cooker);

        for (Cookble cookble : cookbles) {
            cookble.cook();
        }

        ArrayList<Driveble> drivebles = new ArrayList<>();
        drivebles.add((Driveble) driver);

        for (Driveble driveble : drivebles) {
            driveble.drive();
        }

    }
}
