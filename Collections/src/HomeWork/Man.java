package HomeWork;

public class Man {
    private String name;
    private String position;
    private String age;
    private String height;

    public Man(String name, String position, String age, String height) {
        this.name = name;
        this.position = position;
        this.age = age;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public String getAge() {
        return age;
    }

    public String getHeight() {
        return height;
    }
}
