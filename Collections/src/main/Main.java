package main;

import HomeWork.Man;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> Coll1 = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            Coll1.add(i);
        }
        ArrayList<String> Coll2 = new ArrayList<>();
        Coll2.add("Иван");
        Coll2.add("Саша");
        Coll2.add("Ира");
        Coll2.add("Коля");
        Coll2.add("Даня");
        Coll2.add("Никита");
        Coll2.add("Влад");
        Coll2.add("Женя");
        Coll2.add("Ира");
        Coll2.add("Даша");
        ArrayList<String> Coll3 = new ArrayList<>();
        for (int i = 0; i < Coll1.size(); i++) {
            Coll3.add(Coll1.get(i) + " - " + Coll2.get(i));
        }
        for (String name : Coll3) {
            System.out.println(name);
        }
    }
}
