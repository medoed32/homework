public class Main {
    public static void main(String[] args) {
        Director director = new Director();

        director.force(new Counter() {
            @Override
            public String report(int month) {
                return "Отчет за " + month + " месяц(ев)";
            }
        }, 5);

        MyCounter myCounter = new MyCounter();
        director.force(myCounter, 5);
    }
}
