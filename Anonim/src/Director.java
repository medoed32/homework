public class Director {
    public void force(Counter counter, int i){
        System.out.println(counter.report(i));
    }
}
